import React, {Component, Fragment} from 'react';
import Homework from "./Homework/Homework";
import emoji from '../../assets/DabEmoji.jpg';

class Homeworks extends Component {
  state ={
    homeworks: [
      {link: 'https://bitbucket.org/ramilya_nasiza/homework61_ramilya_nasiza/src'},
      {link: 'https://bitbucket.org/ramilya_nasiza/lab60_ramilya_nasiza_azamat_altymyshev/src/master/'},
      {link: 'https://bitbucket.org/ramilya_nasiza/homework59_ramilya_nasiza/src/master/'},
      {link: 'https://bitbucket.org/ramilya_nasiza/homework58_ramilya_nasiza/src/master/'},
      {link: 'https://bitbucket.org/ramilya_nasiza/homework56_57_ramilya_nasiza/src'}
    ]
  };
  render() {
    return (
        <Fragment>
          <h3>Здесь вы можете найти ссылки на мои домашки (я не могла ничего интересного придумать на эту страницу)</h3>
          <img src={emoji} alt=""/>
          {this.state.homeworks.map((homework, index) => (
            <Homework
            work={homework.link}
            numb={index + 1}
            key={index}
            />
          ))}
        </Fragment>
    );
  }
}

export default Homeworks;