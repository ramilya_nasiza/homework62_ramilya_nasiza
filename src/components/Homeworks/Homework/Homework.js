import React from 'react';

const Homework = props => {
  return (
      <div className='Homework'>
        <a href={props.work} style={{fontSize: '30px'}}>Домашка по реакту № {props.numb}</a>
      </div>
  );
};

export default Homework;