import React from 'react';

import './Header.css';
import {NavLink} from "react-router-dom";

const Header = props => {
  return (
      <nav className="navbar navbar-expand-lg navbar-dark bg-nav">
        <div className="container-fluid limit-width">
          <NavLink className="navbar-brand" to='./#'>My page <i className="fas fa-heart"/></NavLink>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                  aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"/>
          </button>
          <div className="collapse navbar-collapse" id="navbarNavDropdown">
            <ul className="main-nav navbar-nav ml-auto">
              <NavLink to='./#'>Home</NavLink>
              <NavLink to='./portfolio'>Portfolio</NavLink>
              <NavLink to='./contacts'>Contacts</NavLink>
            </ul>
          </div>
        </div>
      </nav>
  );
};

export default Header;