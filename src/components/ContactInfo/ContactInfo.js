import React, {Fragment} from 'react';

import './ContactInfo.css';

const ContactInfo = props => {
  return (
      <Fragment>
        <h4>Ну в общем если захотите меня поругать за мою стремную работу, то ниже я указала, как можно со мной связаться</h4>
        <img src={props.emoji} alt=""/>
        <p>Email: <span>nasiza31@gmail.com</span></p>
        <p>Telegram: <span>@milya_nasiza</span></p>
        <p>Work adress: <span>ramilya@sunrisestudio.pro</span></p>
      </Fragment>
  );
};

export default ContactInfo;