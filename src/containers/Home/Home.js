import React, {Component} from 'react';
import Header from "../../components/Header/Header";
import MainInfo from "../../components/MainInfo/MainInfo";
import mainfoto from '../../assets/UNADJUSTEDNONRAW_thumb_aa8.jpg';

import './Home.css';

class Home extends Component {
  render() {
    return (
        <div className='Home'>
          <Header/>
          <MainInfo mainfoto={mainfoto}/>
        </div>
    );
  }
}

export default Home;