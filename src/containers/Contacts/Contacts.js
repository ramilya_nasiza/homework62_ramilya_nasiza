import React, {Component} from 'react';
import ContactInfo from "../../components/ContactInfo/ContactInfo";
import emoji2 from '../../assets/6628_fear.png';

import './Contacts.css';
import Header from "../../components/Header/Header";

class Contacts extends Component {
  render() {
    return (
        <div className='Contacts'>
          <Header/>
          <ContactInfo emoji={emoji2}/>
        </div>
    );
  }
}

export default Contacts;