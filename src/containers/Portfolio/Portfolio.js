import React, {Component} from 'react';
import Homeworks from "../../components/Homeworks/Homeworks";

import './Portfolio.css';
import Header from "../../components/Header/Header";

class Portfolio extends Component {
  render() {
    return (
        <div className='Portfolio'>
          <Header work='./portfolio'/>
          <Homeworks/>
        </div>
    );
  }
}

export default Portfolio;