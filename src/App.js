import React, { Component } from 'react';
import {Route, Switch, BrowserRouter} from "react-router-dom";
import Home from "./containers/Home/Home";

import './App.css';
import Portfolio from "./containers/Portfolio/Portfolio";
import Contacts from "./containers/Contacts/Contacts";

class App extends Component {
  render() {
    return (
        <BrowserRouter>
          <Switch>
            <Route path='/' exact component={Home}/>
            <Route path='/portfolio' exact component={Portfolio}/>
            <Route path='/contacts' exact component={Contacts}/>
          </Switch>
        </BrowserRouter>
    );
  }
}

export default App;
